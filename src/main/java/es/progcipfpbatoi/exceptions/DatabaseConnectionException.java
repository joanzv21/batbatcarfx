package es.progcipfpbatoi.exceptions;

public class DatabaseConnectionException extends RuntimeException {

    public DatabaseConnectionException(String msg) {
        super(msg);
    }

}

