package es.progcipfpbatoi.modelo.dto.types;

public enum EstadoViaje {
    ABIERTO, CERRADO, CANCELADO;

    public static EstadoViaje parse(String estadoViaje) {
        if (estadoViaje.equals("ABIERTO")) {
            return ABIERTO;
        } else if (estadoViaje.equals("CERRADO")) {
            return CERRADO;
        } else if (estadoViaje.equals("CANCELADO")) {
            return CANCELADO;
        } else {
            return null;
        }
     }
}
