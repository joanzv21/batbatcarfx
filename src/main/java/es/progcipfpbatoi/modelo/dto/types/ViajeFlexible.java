package es.progcipfpbatoi.modelo.dto.types;

import es.progcipfpbatoi.exceptions.ReservaNoCancelableException;
import es.progcipfpbatoi.modelo.dto.Reserva;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class ViajeFlexible extends ViajeCancelable {

    public ViajeFlexible(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion) {
        super(codViaje, propietario, ruta, fechaSalida, duracion);
    }

    public ViajeFlexible(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion, float precio, int numPlazas) {
        super(codViaje, propietario, ruta, fechaSalida, duracion, precio, numPlazas);
    }

    public ViajeFlexible(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion, float precio, int numPlazas, EstadoViaje estadoViaje) {
        super(codViaje, propietario, ruta, fechaSalida, duracion, precio, numPlazas, estadoViaje);
    }

    public ViajeFlexible(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion, float precio, int numPlazas, EstadoViaje estadoViaje, ArrayList<Reserva> reservas) {
        super(codViaje, propietario, ruta, fechaSalida, duracion, precio, numPlazas, estadoViaje, reservas);
    }

    @Override
    public String getTypoString() {
        return "Viaje Flexible";
    }

    public Reserva modificarReserva(Reserva reserva, String usuario, int numPlazas) {
         if (haSalido()) {
            throw new ReservaNoCancelableException("La reserva no se puede modificar porque el viaje ya ha salido");
        }
        if (isCancelado()) {
            throw new ReservaNoCancelableException("La reserva no se puede modificar porque el viaje ya ha sido cancelado");
        }

        return super.reservar(reserva.getCodigoReserva(), usuario, numPlazas);
    }
}
