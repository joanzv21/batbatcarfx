package es.progcipfpbatoi.modelo.dto.types;

import es.progcipfpbatoi.exceptions.ReservaNoCancelableException;
import es.progcipfpbatoi.modelo.dto.Reserva;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class ViajeCancelable extends Viaje {

    public ViajeCancelable(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion) {
        super(codViaje, propietario, ruta, fechaSalida, duracion);
    }

    public ViajeCancelable(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion, float precio, int plazas) {
        super(codViaje, propietario, ruta, fechaSalida, duracion, precio, plazas);
    }

    public ViajeCancelable(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion, float precio, int plazas, EstadoViaje estadoViaje) {
        super(codViaje, propietario, ruta, fechaSalida, duracion, precio, plazas, estadoViaje);
    }

    public ViajeCancelable(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion, float precio, int plazas, EstadoViaje estadoViaje, ArrayList<Reserva> reservas) {
        super(codViaje, propietario, ruta, fechaSalida, duracion, precio, plazas, estadoViaje, reservas);
    }

    @Override
    public boolean cancelarReserva(Reserva reserva) {
        if (haSalido()) {
            throw new ReservaNoCancelableException("La reserva no es cancelable porque el viaje ya ha salido");
        }
        if (isCancelado()) {
            throw new ReservaNoCancelableException("La reserva no es cancelable porque el viaje ya ha sido cancelado");
        }
        return reservas.remove(reserva);
    }

    @Override
    public String getTypoString() {
        return "Viaje Cancelable";
    }

}
