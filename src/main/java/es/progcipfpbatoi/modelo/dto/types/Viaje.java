package es.progcipfpbatoi.modelo.dto.types;

import es.progcipfpbatoi.exceptions.*;
import es.progcipfpbatoi.modelo.dto.Reserva;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Objects;

public class Viaje implements Comparable<Viaje>{

    private int codViaje;

    private String propietario;

    private String ruta;

    private LocalDateTime fechaSalida;

    private long duracion;

    private float precio;

    protected int plazasOfertadas;

    protected EstadoViaje estadoViaje;

    protected ArrayList<Reserva> reservas;

    public Viaje(int codViaje) {
        this(codViaje, "", "", LocalDateTime.now(), 0, 5f, 4);
    }

    public Viaje(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion) {
        this(codViaje, propietario, ruta, fechaSalida, duracion, 5f, 4);
    }

    public Viaje(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion,
                 float precio, int plazasOfertadas) {
        this(codViaje, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas, EstadoViaje.ABIERTO);
    }

    public Viaje(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion,
                 float precio, int plazasOfertadas, EstadoViaje estadoViaje) {
        this(codViaje, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas, estadoViaje, new ArrayList<>());
    }

    public Viaje(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion,
                 float precio, int plazasOfertadas, EstadoViaje estadoViaje, ArrayList<Reserva> reservas) {
        this.codViaje = codViaje;
        this.propietario = propietario;
        this.ruta = ruta;
        this.fechaSalida = fechaSalida;
        this.duracion = duracion;
        this.reservas = reservas;
        this.precio = precio;
        this.estadoViaje = estadoViaje;
        setPlazas(plazasOfertadas);
    }

    public void setReservas(ArrayList<Reserva> reservas) {
        this.reservas = reservas;
    }

    public String getTypoString() {
        return "Viaje";
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPlazas(int plazas) {
        if (plazasOfertadas > plazas) {
            throw new PlazasCanNotBeReducedException();
        }
        this.plazasOfertadas = plazas;
    }

    public Reserva reservar(String usuario, int numPlazas) {
        return reservar(getNextCodReserva(), usuario, numPlazas);
    }

    public Reserva reservar(String codReserva, String usuario, int numPlazas) {
        if (usuario.equals(this.propietario)) {
            throw new ReservaNoValidaException("El usuario de la reserva no puede ser igual al propietario");
        }
        if (tieneReservaAnterior(usuario)) {
            throw new ReservaNoValidaException("El usuario ya ha realizado una reserva anteriormente");
        }
        if (isCerrado()) {
            throw new ReservaNoValidaException("El viaje no está abierto");
        }
        if (isCancelado()) {
            throw new ReservaNoValidaException("El viaja está cancelado");
        }
        if (numPlazas > getPlazasDisponibles()) {
            throw new ReservaNoValidaException("No hay suficientes plazas disponibles");
        }
        Reserva reserva = new Reserva(codReserva, usuario, numPlazas);
        reservas.add(reserva);
        if (getPlazasReservadas() == this.plazasOfertadas) {
            cerrarViaje();
        }
        return reserva;
    }

    private String getNextCodReserva() {

        if (reservas.size() == 0) {
            return codViaje + "-" + 0;
        } else {
            int codReserva = Integer.parseInt(reservas.get(reservas.size()-1).getCodigoReserva().split("-")[1]);
            return codViaje + "-" + (codReserva+1);
        }
    }

    private boolean tieneReservaAnterior(String usuario) {
        for (Reserva reserva: reservas) {
            if (reserva.getUsuario().equals(usuario)){
                return true;
            }
        }
        return false;
    }

    public boolean cancelarReserva(Reserva reserva) {
         throw new ReservaNoCancelableException();
    }

    public Reserva getReserva(String codigoReserva) throws ReservaNotFoundException{
        for (Reserva reserva: reservas) {
            if (reserva.getCodigoReserva().equals(codigoReserva)) {
                return reserva;
            }
        }
        throw new ReservaNotFoundException(codigoReserva);
    }

    public boolean isCerrado() {
        return !(this.estadoViaje == EstadoViaje.ABIERTO && !haSalido());
    }

    public int getCodViaje() {
        return codViaje;
    }

    public String getRuta() {
        return ruta;
    }

    public long getDuracion() {
        return duracion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Viaje viaje = (Viaje) o;
        return codViaje == viaje.codViaje;
    }

    @Override
    public int hashCode() {
        return Objects.hash(codViaje);
    }

    public EstadoViaje getEstado() {
        return this.estadoViaje;
    }

    public LocalDateTime getFechaSalida() {
        return fechaSalida;
    }

    public String getFechaSalidaFormatted() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTimeFormatter.format(this.fechaSalida);
    }

    public float getPrecio() {
        return precio;
    }

    public int getPlazasDisponibles() {
        return plazasOfertadas - getPlazasReservadas();
    }

    public int getPlazasOfertadas() {
        return plazasOfertadas;
    }

    private int getPlazasReservadas() {
        int totalPlazasReservadas = 0;
        for (Reserva reserva: reservas) {
            totalPlazasReservadas += reserva.getPlazasSolicitadas();
        }
        return totalPlazasReservadas;
    }

    @Override
    public int compareTo(Viaje o) {
        if (codViaje == o.codViaje) {
            return 0;
        } else if (codViaje > o.codViaje) {
            return 1;
        } else {
            return -1;
        }
    }

    public ArrayList<Reserva> getReservas() {
        return reservas;
    }

    public boolean estaDisponible() {
        return this.estadoViaje == EstadoViaje.ABIERTO && !haSalido();
    }

    protected boolean haSalido() {
        return fechaSalida.isBefore(LocalDateTime.now());
    }

    public boolean isCancelado() {
        return this.estadoViaje == EstadoViaje.CANCELADO;
    }

    public void cancelar() throws ViajeNotCancelableException{
        if (!estaDisponible()) {
            throw new ViajeNotCancelableException(String.valueOf(this.codViaje));
        }
        this.estadoViaje = EstadoViaje.CANCELADO;
    }

    public boolean tieneEsteEstado(EstadoViaje estadoViaje) {
        return this.estadoViaje == estadoViaje;
    }

    public boolean tieneEsteCiudadDestino(String ciudadDestino) {
        return this.ruta.substring(this.ruta.indexOf("-")).contains(ciudadDestino);
    }

    public Reserva modificarReserva(Reserva reserva, String usuario, int numPlazas) {
         throw new ReservaNoModificableException();
    }

    public void cerrarViaje() {
        estadoViaje = EstadoViaje.CERRADO;
    }

    @Override
    public String toString() {
        return "Viaje{" +
                "tipo=" + getTypoString() +
                ", codViaje=" + codViaje +
                ", propietario='" + propietario + '\'' +
                ", ruta='" + ruta + '\'' +
                ", fechaSalida=" + fechaSalida +
                ", plazasOfertadas=" + plazasOfertadas +
                '}';
    }
}
