package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.exceptions.ReservaNotFoundException;
import es.progcipfpbatoi.modelo.dto.Reserva;
import es.progcipfpbatoi.modelo.dto.types.Viaje;

import java.util.ArrayList;

public class DatabaseReservaDAO implements RerservaDAOInterface {
    @Override
    public ArrayList<Reserva> findAll() {
        return null;
    }

    @Override
    public Reserva findById(String id) {
        return null;
    }

    @Override
    public ArrayList<Reserva> findByUser(String user) {
        return null;
    }

    @Override
    public ArrayList<Reserva> findByTravel(String codViaje) {
        return null;
    }

    @Override
    public ArrayList<Reserva> findByTravel(Viaje viaje) {
        return null;
    }


    @Override
    public Reserva getById(String id) throws ReservaNotFoundException {
        return null;
    }

    @Override
    public boolean save(Reserva reserva) {
        return false;
    }

    @Override
    public boolean remove(Reserva reserva) {
        return false;
    }
}
