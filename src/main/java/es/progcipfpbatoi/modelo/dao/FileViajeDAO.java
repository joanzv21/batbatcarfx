package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.exceptions.ViajeNotFoundException;
import es.progcipfpbatoi.modelo.dto.types.*;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.TreeSet;

public class FileViajeDAO implements ViajeDAOInterface{

    private static final String DATABASE_FILE = "resources/database/viajes.txt";
    private static final String FIELD_SEPARATOR = ";";

    private static final int COLUM_TIPO = 0;

    private static final int COLUMN_COD = 1;

    private static final int COLUMN_PROPIETARIO = 2;

    private static final int COLUMN_RUTA = 3;

    private static final int COLUMN_FECHA = 4;

    private static final int COLUMN_DURACION = 5;

    private static final int COLUMN_PRECIO = 6;

    private static final int COLUMN_PLAZAS = 7;

    private static final int COLUMN_ESTADO = 8;

    private File file;

    public FileViajeDAO() {

        this.file = new File(DATABASE_FILE);
    }

    public FileViajeDAO(String pathToFile) {
        this.file = new File(getClass().getResource(pathToFile).getFile());
    }

    @Override
    public Set<Viaje> findAll() {
        try {
            Set<Viaje> viajes = new TreeSet<>();
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                do {
                    String viajeRegistro = bufferedReader.readLine();
                    if (viajeRegistro == null) {
                        return viajes;
                    }
                    Viaje viaje = getViajeFromRegister(viajeRegistro);
                    viajes.add(viaje);
                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    private String getRegisterFromViaje(Viaje viaje) {
        String[] fields = new String[9];
        fields[COLUM_TIPO] = getTipo(viaje);
        fields[COLUMN_COD] = String.valueOf(viaje.getCodViaje());
        fields[COLUMN_PROPIETARIO] = viaje.getPropietario();
        fields[COLUMN_RUTA] =  viaje.getRuta();
        fields[COLUMN_FECHA] =  viaje.getFechaSalidaFormatted();
        fields[COLUMN_DURACION] =  String.valueOf(viaje.getDuracion());
        fields[COLUMN_PRECIO] =  String.valueOf(viaje.getPrecio());
        fields[COLUMN_PLAZAS] =  String.valueOf(viaje.getPlazasOfertadas());
        fields[COLUMN_ESTADO] =  viaje.getEstado().toString();
        return String.join(FIELD_SEPARATOR, fields);
    }

    private String getTipo(Viaje viaje) {
        String tipoViaje = viaje.getTypoString();
        if (tipoViaje.equals("Viaje")) {
            return "VIAJE";
        } else if (tipoViaje.equals("Viaje Flexible")) {
            return "VIAJEFLEXIBLE";
        } else if (tipoViaje.equals("Viaje Exclusivo")) {
            return "VIAJEEXCLUSIVO";
        } else if (tipoViaje.equals("Viaje Cancelable")) {
            return "VIAJECANCELABLE";
        } else {
            return null;
        }
    }

    private Viaje getViajeFromRegister(String viajeRegistro) {
        String[] viajeFields = viajeRegistro.split(FIELD_SEPARATOR);
        String tipoViaje = viajeFields[COLUM_TIPO];
        int codViaje = Integer.parseInt(viajeFields[COLUMN_COD]);
        String propietario = viajeFields[COLUMN_PROPIETARIO];
        String ruta = viajeFields[COLUMN_RUTA];
        LocalDateTime fechaDeSalida = LocalDateTime.parse(viajeFields[COLUMN_FECHA], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        int duracion = Integer.parseInt(viajeFields[COLUMN_DURACION]);
        float precio = Float.parseFloat(viajeFields[COLUMN_PRECIO]);
        int numPlazas = Integer.parseInt(viajeFields[COLUMN_PLAZAS]);
        EstadoViaje estadoViaje = EstadoViaje.parse(viajeFields[COLUMN_ESTADO]);
        return getViajeOfType(tipoViaje, codViaje, propietario, ruta, fechaDeSalida, duracion, precio, numPlazas, estadoViaje);
    }

    private Viaje getViajeOfType(String tipoViaje, int codViaje, String propietario, String ruta,
                                 LocalDateTime fechaDeSalida, int duracion, float precio, int numPlazas, EstadoViaje estadoViaje) {
        if (tipoViaje.equals("VIAJE")) {
            return new Viaje(codViaje, propietario, ruta, fechaDeSalida, duracion, precio, numPlazas, estadoViaje);
        } else if (tipoViaje.equals("VIAJEFLEXIBLE")) {
            return new ViajeFlexible(codViaje, propietario, ruta, fechaDeSalida, duracion, precio, numPlazas, estadoViaje);
        } else if (tipoViaje.equals("VIAJEEXCLUSIVO")) {
            return new ViajeExclusivo(codViaje, propietario, ruta, fechaDeSalida, duracion, precio, numPlazas, estadoViaje);
        } else if (tipoViaje.equals("VIAJECANCELABLE")) {
            return new ViajeCancelable(codViaje, propietario, ruta, fechaDeSalida, duracion, precio, numPlazas, estadoViaje);
        } else {
            return null;
        }
    }

    @Override
    public Set<Viaje> findAll(String city) {
        try {
            Set<Viaje> viajes = new TreeSet<>();
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String viajeRegistro = bufferedReader.readLine();
                    if (viajeRegistro == null) {
                        return viajes;
                    }

                    Viaje viaje = getViajeFromRegister(viajeRegistro);
                    if (viaje.tieneEsteCiudadDestino(city)) {
                        viajes.add(viaje);
                    }

                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Set<Viaje> findAll(EstadoViaje estadoViaje) {
        try {
            Set<Viaje> viajes = new TreeSet<>();
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String viajeRegistro = bufferedReader.readLine();
                    if (viajeRegistro == null) {
                        return viajes;
                    }

                    Viaje viaje = getViajeFromRegister(viajeRegistro);
                    if (viaje.tieneEsteEstado(estadoViaje)) {
                        viajes.add(viaje);
                    }
                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Set<Viaje> findAll(Class<? extends Viaje> viajeClass) {
        try {
            Set<Viaje> viajes = new TreeSet<>();
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String viajeRegistro = bufferedReader.readLine();
                    if (viajeRegistro == null) {
                        return viajes;
                    }

                    Viaje viaje = getViajeFromRegister(viajeRegistro);
                    if (viaje.getClass() == viajeClass) {
                        viajes.add(viaje);
                    }

                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Viaje getById(int codViaje) throws ViajeNotFoundException{

        try {
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String viajeRegistro = bufferedReader.readLine();
                    if (viajeRegistro == null) {
                        throw new ViajeNotFoundException(String.valueOf(codViaje));
                    }

                    Viaje viaje = getViajeFromRegister(viajeRegistro);
                    if (viaje.getCodViaje() == codViaje) {
                        return viaje;
                    }

                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Viaje findById(int codViaje) {

        try {
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String viajeRegistro = bufferedReader.readLine();
                    if (viajeRegistro == null) {
                        return null;
                    }

                    Viaje viaje = getViajeFromRegister(viajeRegistro);
                    if (viaje.getCodViaje() == codViaje) {
                        return viaje;
                    }

                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Viaje getById(String codViaje, String ciudadDestino) throws ViajeNotFoundException {

        Set<Viaje> viajesEnDestino = findAll(ciudadDestino);

        for (Viaje viaje : viajesEnDestino) {
            if (viaje.getCodViaje() == Integer.parseInt(codViaje)) {
                return viaje;
            }
        }
        throw new ViajeNotFoundException(String.valueOf(codViaje));

    }

    @Override
    public Viaje getModificableById(String codViaje) throws ViajeNotFoundException {
        Set<Viaje> viajesModificables = findAll(ViajeFlexible.class);
        for (Viaje viaje : viajesModificables) {
            if (viaje.getCodViaje() == Integer.parseInt(codViaje)) {
                return viaje;
            }
        }

        throw new ViajeNotFoundException(String.valueOf(codViaje));
    }

    @Override
    public boolean save(Viaje viaje) {
        try {
            if (findById(viaje.getCodViaje()) == null) {
                append(viaje);
                return true;
            }
            return updateOrRemove(viaje, true);
        }catch (IOException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean remove(Viaje viaje) {
        try {
            return updateOrRemove(viaje, false);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    private boolean updateOrRemove(Viaje viaje, boolean update) throws IOException {
        Set<Viaje> viajesList = findAll();
        try (BufferedWriter bufferedWriter = getWriter(false)) {
            for (Viaje viajeItem : viajesList) {
                if (!viajeItem.equals(viaje)) {
                    bufferedWriter.write(getRegisterFromViaje(viajeItem));
                    bufferedWriter.newLine();
                } else if (update) {
                    bufferedWriter.write(getRegisterFromViaje(viaje));
                    bufferedWriter.newLine();
                }
            }
        }
        return true;
    }

    private void append(Viaje viaje) throws IOException {
        try (BufferedWriter bufferedWriter = getWriter(true)) {
            bufferedWriter.write(getRegisterFromViaje(viaje));
            bufferedWriter.newLine();
        }
    }

    private BufferedWriter getWriter(boolean append) throws IOException {
        return  new BufferedWriter(new FileWriter(file, append));
    }

    private BufferedReader getReader() throws IOException {
        return new BufferedReader(new FileReader(file));
    }
}
