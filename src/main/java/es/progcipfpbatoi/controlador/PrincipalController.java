package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.exceptions.DatabaseConnectionException;
import es.progcipfpbatoi.modelo.dao.ViajeDAOInterface;
import es.progcipfpbatoi.modelo.dto.Reserva;
import es.progcipfpbatoi.modelo.dto.types.Viaje;
import es.progcipfpbatoi.modelo.repositorios.ViajesRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;

public class PrincipalController implements GenericController{

    private final ViajesRepository viajesRepository;




    public PrincipalController(ViajesRepository viajesRepository) {
        this.viajesRepository = viajesRepository;
    }

    @FXML
    private ListView<Viaje> taskListView;





    @FXML
    public void initialize() {

        taskListView.setItems(getData());
        taskListView.setCellFactory((ListView<Viaje> l) -> new ViajeVistaController());

    }
    private ObservableList<Viaje> getData() {
        try {
            return FXCollections.observableArrayList(viajesRepository.findAll());
        } catch (DatabaseConnectionException ex) {
            System.out.println(ex.getMessage());
            return FXCollections.observableArrayList(new ArrayList<>());
        }
    }

    @FXML
    private void handleSelectedItem(MouseEvent event) {

        /*
            El siguiente código, cambia el estado de una tarea. Es un código alternativo al que está comentado

            Tarea tarea = taskListView.getSelectionModel().getSelectedItem();
            tarea.cambiaEstado();
            this.tareaDAOInterfaz.save(tarea);
            taskListView.getSelectionModel().clearSelection();
            taskListView.refresh();
        */


            Stage stage = (Stage) ((Node) ((EventObject) event).getSource()).getScene().getWindow();
            Viaje viaje = taskListView.getSelectionModel().getSelectedItem();
            ViajeDetailController viajeDetailController = new ViajeDetailController(viajesRepository, viaje);
            ChangeScene.change(stage, viajeDetailController, "/vista/tarea_detail.fxml");

    }
}
