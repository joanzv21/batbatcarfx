package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.modelo.dto.types.Viaje;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URISyntaxException;

public class ViajeVistaController  extends ListCell<Viaje> {

    @FXML
    private AnchorPane root;

    @FXML
    private ImageView ImageVIewtTipo;

    @FXML
    private TextField TextFieldRuta;

    @FXML
    private TextField TextFieldPrecio;

    @FXML
    private TextField TextfieldFecha;

    @FXML
    private TextField TextFieldPlazas;

    @FXML
    private ImageView ImageViewDisponible;


    public ViajeVistaController() {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/ViajeVista.fxml"));
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    @Override
    protected void updateItem(Viaje viaje, boolean empty) {
        super.updateItem(viaje, empty);

        if (empty) {
            setGraphic(null);
        } else {
            TextFieldRuta.setText(viaje.getRuta());

            ImageViewDisponible.setImage(getCategoryImage());

            TextFieldPrecio.setText(String.valueOf(viaje.getPrecio()));

            TextfieldFecha.setText(String.valueOf(viaje.getFechaSalida()));

            TextFieldPlazas.setText(String.valueOf(viaje.getPlazasDisponibles()));

            ImageVIewtTipo.setImage(getCategoryImage());


            setGraphic(root);
        }



    }

    private String getPathImage(String fileName) {
        try {
            return getClass().getResource(fileName).toURI().toString();
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private Image getCategoryImage() {
                return new Image(getPathImage("/images/play.png"));
        }
    }

