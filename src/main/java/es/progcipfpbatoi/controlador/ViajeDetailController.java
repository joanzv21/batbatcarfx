package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.modelo.dto.types.Viaje;
import es.progcipfpbatoi.modelo.repositorios.ViajesRepository;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.EventObject;

public class ViajeDetailController implements GenericController{

    @FXML
    private TextField tipoViaje;

    @FXML
    private TextField precioViaje;

    @FXML
    private TextField usuarioViaje;

    @FXML
    private TextField origenViaje;

    @FXML
    private TextField destinoViaje;

    @FXML
    private TextField diaDeSalida;

    @FXML
    private TextField plazasViajes;

    @FXML
    private Button botonAtras;




    private final ViajesRepository viajesRepository;
    private Viaje viaje;


    public ViajeDetailController(ViajesRepository viajesRepository, Viaje viaje) {
        this.viajesRepository = viajesRepository;
        this.viaje = viaje;
    }

    @Override
    public void initialize() {



        tipoViaje.setText(viaje.getTypoString());
        precioViaje.setText(String.valueOf(viaje.getPrecio()));
        usuarioViaje.setText(viaje.getPropietario());
        plazasViajes.setText(String.valueOf(viaje.getPlazasOfertadas()));


    }


    @FXML
    private void botonAtras(ActionEvent event) {

            Stage stage = (Stage) ((Node) ((EventObject) event).getSource()).getScene().getWindow();
            PrincipalController viajeDetailController = new PrincipalController(viajesRepository);
            ChangeScene.change(stage, viajeDetailController, "/vista/principal.fxml");

    }
}
