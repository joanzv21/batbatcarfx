package es.progcipfpbatoi;

import es.progcipfpbatoi.exceptions.ViajeNotFoundException;
import es.progcipfpbatoi.modelo.dao.FileViajeDAO;
import es.progcipfpbatoi.modelo.dto.types.*;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class TestFileViajeDAO extends TestFileGenericDAO{

    private FileViajeDAO fileViajeDAO;

    public static final String[] FILE_INITIAL_REGISTERS = {
            "VIAJE;1;Roberto;Alicante-Ibi;2022-08-03 19:59:45;30;23.5;8;ABIERTO",
            "VIAJEFLEXIBLE;2;Alex;Alcoi-Cocentaina;2022-05-03 12:00:00;5;5;10;ABIERTO",
            "VIAJECANCELABLE;3;Luis;Alcoi-Valencia;2022-10-03 20:00:00;20;40.2;3;CANCELADO",
            "VIAJEEXCLUSIVO;4;Juan;Alicante-Valencia;2022-10-01 20:00:00;22;10.5;5;CERRADO",
            "VIAJECANCELABLE;5;Paco;Valencia-Alcoi;2022-10-03 20:00:00;20;40.2;3;ABIERTO"
    };

    public static final String PATH_TO_TEST_FILE = "/database/viajes.txt";

    @BeforeEach
    public void prepareTestFile() {
        prepareTestDBFile(PATH_TO_TEST_FILE, FILE_INITIAL_REGISTERS);
    }

    public TestFileViajeDAO () {
        this.fileViajeDAO = new FileViajeDAO(PATH_TO_TEST_FILE);
    }

    @Test
    public void testFindAll() {
        Set<Viaje> viajes = this.fileViajeDAO.findAll();
        assertEquals(FILE_INITIAL_REGISTERS.length, viajes.size());
        int codigoViajeEsperado = 1;
        for (Viaje viaje: viajes) {
            assertEquals(codigoViajeEsperado++, viaje.getCodViaje());
        }
    }

    @Test
    public void testFindCityFound() {
        String ciudad = "Valencia";
        Set<Viaje> viajesHaciaValencia = this.fileViajeDAO.findAll(ciudad);
        assertEquals(viajesHaciaValencia.size(), 2);
    }

    @Test
    public void testFindCityNotFound() {
        String ciudad = "Muro de Alcoy";
        Set<Viaje> viajesHaciaValencia = this.fileViajeDAO.findAll(ciudad);
        assertEquals(viajesHaciaValencia.size(), 0);
    }

    @Test
    public void testFindEstadoAbierto() {
        Set<Viaje> viajes = this.fileViajeDAO.findAll(EstadoViaje.ABIERTO);
        assertEquals(viajes.size(), 3);
        for (Viaje viaje: viajes) {
            assertEquals(viaje.getEstado(),  EstadoViaje.ABIERTO);
        }
    }

    @Test
    public void testFindEstadoCancelado() {
        Set<Viaje> viajes = this.fileViajeDAO.findAll(EstadoViaje.CANCELADO);
        assertEquals(viajes.size(), 1);
        for (Viaje viaje : viajes) {
            assertEquals(viaje.getEstado(), EstadoViaje.CANCELADO);
        }
    }

    @Test
    public void testFindEstadoCerrado() {
        Set<Viaje> viajes = this.fileViajeDAO.findAll(EstadoViaje.CERRADO);
        assertEquals(viajes.size(), 1);
        for (Viaje viaje : viajes) {
            assertEquals(viaje.getEstado(), EstadoViaje.CERRADO);
        }
    }

    @Test
    public void testFindViajeCancelable() {
        Set<Viaje> viajes = this.fileViajeDAO.findAll(ViajeCancelable.class);
        assertEquals(viajes.size(), 2);
        for (Viaje viaje : viajes) {
            assertInstanceOf(ViajeCancelable.class, viaje);
        }
    }

    @Test
    public void testFindViajeFlexible() {
        Set<Viaje> viajes = this.fileViajeDAO.findAll(ViajeFlexible.class);
        assertEquals(viajes.size(), 1);
        for (Viaje viaje : viajes) {
            assertInstanceOf(ViajeFlexible.class, viaje);
        }
    }

    @Test
    public void testFindViajeExclusivo() {
        Set<Viaje> viajes = this.fileViajeDAO.findAll(ViajeExclusivo.class);
        assertEquals(viajes.size(), 1);
        for (Viaje viaje : viajes) {
            assertInstanceOf(ViajeExclusivo.class, viaje);
        }
    }

    @Test
    public void testFindViajeNormal() {
        Set<Viaje> viajesNormales = this.fileViajeDAO.findAll(Viaje.class);
        assertEquals(viajesNormales.size(), 1);
    }

    @Test
    public void testGetByIdExistRegister() {
        int codViaje = 3;
        Viaje viaje = null;
        try {
            viaje = this.fileViajeDAO.getById(codViaje);
        } catch (ViajeNotFoundException e) {
            fail();
        }
        assertEquals(codViaje, viaje.getCodViaje());
    }

    @Test
    public void testGetByIdNotExistRegister() {
        assertThrows(ViajeNotFoundException.class, () -> {
            this.fileViajeDAO.getById(9);
        });
    }

    @Test
    public void testNewSaveInsert() {
        int codigoTest = 6;
        String propietario = "Arturo";
        String ruta = "Valencia-Alicante";
        LocalDateTime fechaSalida =  LocalDateTime.of(2023, 5, 12, 12, 0, 0, 0);
        int duracion = 120;
        int precio = 30;
        int plazasOfertadas = 5;
        Viaje viajeToSave2 = new Viaje(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
        this.fileViajeDAO.save(viajeToSave2);
        testViajeEquals(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
    }

    @Test
    public void testNewViajeExclusivoSaveInsert() {
        int codigoTest = 6;
        String propietario = "Arturo";
        String ruta = "Valencia-Alicante";
        LocalDateTime fechaSalida =  LocalDateTime.of(2023, 5, 12, 12, 0, 0, 0);
        int duracion = 120;
        int precio = 30;
        int plazasOfertadas = 5;
        Viaje viajeToSave2 = new ViajeExclusivo(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
        this.fileViajeDAO.save(viajeToSave2);
        testViajeEquals(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
    }

    @Test
    public void testNewViajeCancelableSaveInsert() {
        int codigoTest = 6;
        String propietario = "Arturo";
        String ruta = "Valencia-Alicante";
        LocalDateTime fechaSalida =  LocalDateTime.of(2023, 5, 12, 12, 0, 0, 0);
        int duracion = 120;
        int precio = 30;
        int plazasOfertadas = 5;
        Viaje viajeToSave2 = new ViajeCancelable(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
        this.fileViajeDAO.save(viajeToSave2);
        testViajeEquals(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
    }

    @Test
    public void testUpdateSaveOK() {
        int codigoTest = 1;
        String propietario = "Arturo";
        String ruta = "Valencia-Alicante";
        LocalDateTime fechaSalida =  LocalDateTime.of(2023, 5, 12, 12, 0, 0, 0);
        int duracion = 120;
        int precio = 30;
        int plazasOfertadas = 5;
        Viaje viajeToSave2 = new Viaje(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
        this.fileViajeDAO.save(viajeToSave2);
        testViajeEquals(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
    }

    private void testViajeEquals(int codigoTest, String propietario, String ruta,
                                 LocalDateTime fechaSalida, int duracion, int precio, int plazasOfertadas) {
        try {
            Viaje viaje = this.fileViajeDAO.getById(codigoTest);
            assertEquals(viaje.getCodViaje(), codigoTest, "codigo no se establece correctamente");
            assertEquals(viaje.getPropietario(), propietario, "propietario no se establece correctamente");
            assertEquals(viaje.getRuta(), ruta, "ruta no se establece correctamente");
            assertTrue(viaje.getFechaSalida().isEqual(fechaSalida), "fecha no se establece correctamente");
            assertEquals(viaje.getDuracion(), duracion, "duracion no se establece correctamente");
            assertEquals(viaje.getPrecio(), precio, "precio no se establece correctamente");
            assertEquals(viaje.getPlazasOfertadas(), plazasOfertadas, "plazas ofertadas no se establece correctamente");
        } catch (ViajeNotFoundException ex) {
            fail();
        }
    }

    @Test
    public void testRemove() {
        int codViajeTest = 1;
        this.fileViajeDAO.remove(new Viaje(codViajeTest));
        Viaje viaje = this.fileViajeDAO.findById(codViajeTest);
        assertNull(viaje, "La función de borrar viaje es incorrecta");
    }
}
