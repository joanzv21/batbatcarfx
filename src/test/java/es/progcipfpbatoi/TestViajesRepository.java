package es.progcipfpbatoi;

import es.progcipfpbatoi.exceptions.ReservaNotFoundException;
import es.progcipfpbatoi.exceptions.ViajeNotCancelableException;
import es.progcipfpbatoi.exceptions.ViajeNotFoundException;
import es.progcipfpbatoi.modelo.dao.FileReservaDAO;
import es.progcipfpbatoi.modelo.dao.FileViajeDAO;
import es.progcipfpbatoi.modelo.dto.Reserva;
import es.progcipfpbatoi.modelo.dto.types.*;
import es.progcipfpbatoi.modelo.repositorios.ViajesRepository;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class TestViajesRepository extends TestFileGenericDAO{

    private ViajesRepository viajesRepository;

    @BeforeEach
    public void prepareTestFile() {
        prepareTestDBFile(TestFileReservaDAO.PATH_TO_TEST_FILE, TestFileReservaDAO.FILE_INITIAL_REGISTERS);
        prepareTestDBFile(TestFileViajeDAO.PATH_TO_TEST_FILE, TestFileViajeDAO.FILE_INITIAL_REGISTERS);
    }

    public TestViajesRepository() {
        FileViajeDAO fileViajeDAO = new FileViajeDAO(TestFileViajeDAO.PATH_TO_TEST_FILE);
        FileReservaDAO fileReservaDAO = new FileReservaDAO(TestFileReservaDAO.PATH_TO_TEST_FILE);
        this.viajesRepository = new ViajesRepository(fileViajeDAO, fileReservaDAO);
    }

    @Test
    public void testCountViajes() {
        assert (this.viajesRepository.countViajes() == TestFileViajeDAO.FILE_INITIAL_REGISTERS.length);
    }

    @Test
    public void testGetViaje() {
        try {
            int codViaje = 3;
            Viaje viaje = this.viajesRepository.getViaje(codViaje);
            assertTrue(viaje.getCodViaje() == codViaje);
        } catch (ViajeNotFoundException ex) {
            fail();
        }

        try {
            int codViaje = 9;
            this.viajesRepository.getViaje(codViaje);
            fail();
        } catch (ViajeNotFoundException ex) {
            assertTrue(true);
        }
    }

    @Test
    public void testGetViajeInDestination() {

        try {
            int codViaje = 3;
            this.viajesRepository.getViaje(codViaje, "Mallorca");
            fail();
        } catch (ViajeNotFoundException ex) {
            assertTrue(true);
        }

        try {
            int codViaje = 3;
            this.viajesRepository.getViaje(codViaje, "Ibi");
            fail();
        } catch (ViajeNotFoundException ex) {
            assertTrue(true);
        }

        try {
            int codViaje = 3;
            Viaje viaje = this.viajesRepository.getViaje(codViaje, "Valencia");
            assertTrue(viaje.getCodViaje() == codViaje);
        } catch (ViajeNotFoundException ex) {
            fail();
        }
    }

    @Test
    public void testGetViajeModificable() {
        try {
            int codViaje = 2;
            Viaje viaje = this.viajesRepository.getViajeModificable(codViaje);
            assertTrue(viaje.getCodViaje() == codViaje);
        } catch (ViajeNotFoundException ex) {
            fail();
        }

        try {
            int codViaje = 3;
            this.viajesRepository.getViajeModificable(codViaje);
            fail();
        } catch (ViajeNotFoundException ex) {
            assertTrue(true);
        }

        try {
            int codViaje = 9;
            this.viajesRepository.getViajeModificable(codViaje);
            fail();
        } catch (ViajeNotFoundException ex) {
            assertTrue(true);
        }
    }

    @Test
    public void testFindAll() {
        Set<Viaje> viajes = this.viajesRepository.findAll();
        assertTrue(viajes.size() == TestFileViajeDAO.FILE_INITIAL_REGISTERS.length);
    }

    @Test
    public void testFindAllCity() {
        Set<Viaje> viajes = this.viajesRepository.findAll("Ciudad2");
        assertTrue(viajes.size() == 0);

        Set<Viaje> viajes2 = this.viajesRepository.findAll("Valencia");
        assertTrue(viajes2.size() == 2);
    }

    @Test
    public void testFindAllEstado() {
        Set<Viaje> viajesAbiertos = this.viajesRepository.findAll(EstadoViaje.ABIERTO);
        assertTrue(viajesAbiertos.size() == 3);

        Set<Viaje> viajesCancelados = this.viajesRepository.findAll(EstadoViaje.CANCELADO);
        assertTrue(viajesCancelados.size() == 1);

        Set<Viaje> viajesCerrados = this.viajesRepository.findAll(EstadoViaje.CERRADO);
        assertTrue(viajesCerrados.size() == 1);
    }

    @Test
    public void testFindAllClass() {
        Set<Viaje> viajesCancelables = this.viajesRepository.findAll(ViajeCancelable.class);
        assertTrue(viajesCancelables.size() == 2);

        Set<Viaje> viajesFlexibles = this.viajesRepository.findAll(ViajeFlexible.class);
        assertTrue(viajesFlexibles.size() == 1);

        Set<Viaje> viajesExclusivos = this.viajesRepository.findAll(ViajeExclusivo.class);
        assertTrue(viajesExclusivos.size() == 1);

        Set<Viaje> viajesNormales = this.viajesRepository.findAll(Viaje.class);
        assertTrue(viajesNormales.size() == 1);
    }

    @Test
    public void testGetViajeConReserva() {
        try {
            String codReserva = "1-1";
            Viaje viaje = this.viajesRepository.getViajeConReserva(codReserva);
            assertEquals(1, viaje.getCodViaje());
        } catch (ViajeNotFoundException | ReservaNotFoundException ex) {
            fail();
        }

        try {
            String codReserva = "1-3";
            this.viajesRepository.getViajeConReserva(codReserva);
            fail();
        } catch (ViajeNotFoundException ex) {
            fail();
        } catch (ReservaNotFoundException ex) {
            assertTrue(true);
        }

        try {
            String codReserva = "0-1";
            this.viajesRepository.getViajeConReserva(codReserva);
            fail();
        } catch (ViajeNotFoundException ex) {
            assertTrue(true);
        } catch (ReservaNotFoundException ex) {
            fail();
        }
    }

    @Test
    public void testAnyadir() {

        int codigoNuevoViaje = 6;
        ArrayList<Reserva> reservas = new ArrayList<>();
        reservas.add(new Reserva(codigoNuevoViaje + "-" + 0, "pepe", 1));
        reservas.add(new Reserva(codigoNuevoViaje + "-" + 1, "pepa", 2));
        Viaje viaje = new Viaje(codigoNuevoViaje, "test", "Ciudad1-Ciudad2", LocalDateTime.now(), 12, 5f, 10, EstadoViaje.ABIERTO, reservas);
        assert (this.viajesRepository.countViajes() == TestFileViajeDAO.FILE_INITIAL_REGISTERS.length);
        this.viajesRepository.anyadir(viaje);
        assert (this.viajesRepository.countViajes() == (TestFileViajeDAO.FILE_INITIAL_REGISTERS.length + 1));

        try {
            Viaje nuevoViaje = this.viajesRepository.getViaje(codigoNuevoViaje);
            assertEquals(2, nuevoViaje.getReservas().size());
        } catch (ViajeNotFoundException ex) {
            fail();
        }
    }

    @Test
    public void testReservar() {
        try {
            Viaje viaje = this.viajesRepository.getViaje(1);
            this.viajesRepository.reservar(viaje, "pepe", 1);
            Viaje viajeConReserva = this.viajesRepository.getViajeConReserva("1-3");
            assertEquals(3, viajeConReserva.getReservas().size());
            assertEquals(0, viajeConReserva.getPlazasDisponibles());
        } catch (ViajeNotFoundException | ReservaNotFoundException ex) {
            fail();
        }
    }

    @Test
    public void testCancelarReserva() {
        String codigoReserva = "2-1";
        try {
            Viaje viaje = this.viajesRepository.getViajeConReserva(codigoReserva);
            assertEquals(2, viaje.getCodViaje());
        } catch (ViajeNotFoundException | ReservaNotFoundException ex) {
            fail();
        }
        this.viajesRepository.cancelarReserva(codigoReserva);
        try {
            this.viajesRepository.getViajeConReserva(codigoReserva);
            fail();
        } catch (ViajeNotFoundException ex) {
            fail();
        } catch (ReservaNotFoundException ex) {
            assertTrue(true);
        }
    }

    @Test
    public void testModificarReserva() {
        try {
            String codReservaTest = "2-2";
            Reserva segundaReservaViaje2 = this.viajesRepository.getViajeConReserva(codReservaTest).getReservas().get(1);
            assertNotNull(segundaReservaViaje2);
            String usuarioAntesDeModificar = segundaReservaViaje2.getUsuario();
            assertEquals("roberto", usuarioAntesDeModificar);
            int plazasSolicitadas = segundaReservaViaje2.getPlazasSolicitadas();
            assertEquals(5, plazasSolicitadas);
            String usuarioReemplazo = "manuel";
            int nuevasPlazasReserva = 6;
            this.viajesRepository.modificarReserva(codReservaTest, usuarioReemplazo, nuevasPlazasReserva);

            Reserva segundaReservaViaje2Actualizada = this.viajesRepository.getViajeConReserva(codReservaTest).getReservas().get(1);
            assertNotNull(segundaReservaViaje2Actualizada);
            assertEquals(usuarioReemplazo, segundaReservaViaje2Actualizada.getUsuario());
            assertEquals(nuevasPlazasReserva, segundaReservaViaje2Actualizada.getPlazasSolicitadas());
            assertNotNull(segundaReservaViaje2);
        } catch(ViajeNotFoundException | ReservaNotFoundException ex) {
            fail();
        }
    }

    @Test
    public void testCancelarViaje() {
        int codViajeACancelar = 5;
        try {

            assertFalse(this.viajesRepository.getViaje(codViajeACancelar).isCancelado());
            this.viajesRepository.cancelarViaje(codViajeACancelar);
            assertTrue(this.viajesRepository.getViaje(codViajeACancelar).isCancelado());
        } catch (ViajeNotFoundException | ViajeNotCancelableException ex) {
            fail();
        }

        try {
            this.viajesRepository.cancelarViaje(codViajeACancelar);
            fail();
        } catch (ViajeNotFoundException ex)  {
            fail();
        }  catch (ViajeNotCancelableException ex) {
            assertTrue(true);
        }

        try {
            this.viajesRepository.cancelarViaje(9);
            fail();
        } catch (ViajeNotFoundException ex)  {
            assertTrue(true);
        }  catch (ViajeNotCancelableException ex) {
            fail();
        }
    }
}
