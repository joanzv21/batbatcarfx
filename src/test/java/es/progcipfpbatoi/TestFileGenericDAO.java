package es.progcipfpbatoi;

import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class TestFileGenericDAO {

    protected static void prepareTestDBFile(String testFilePath, String[] initialRegisters) {
        try {
            File file = new File(TestFileGenericDAO.class.getResource(testFilePath).getFile());
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, false))) {
                for (String registerReserva: initialRegisters) {
                    bufferedWriter.write(registerReserva);
                    bufferedWriter.newLine();
                }
            }
        }catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

}

